import React from 'react';
import CreditFraudComponent from './components/CreditFraudComponent';
import NavBarComponent from './components/Navbar';
import FooterComponent from './components/Footer';
import AboutUs from './components/AboutUs';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import './App.css';
import { Route, Switch } from 'react-router-dom';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#0388A6',
      main: '#024059',
      dark: '#012c3e',
      contrastText: '#fff',
    },
    secondary: {
      light: '#35abb7',
      main: '#0396A6',
      dark: '#026974',
      contrastText: '#fff',
    },
  },
});

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <Switch>
          {/* Credit Fraud main component */}
          <Route exact path='/'>
            <NavBarComponent />
            <CreditFraudComponent />
            <FooterComponent />
          </Route>
          {/* About Us component */}
          <Route path='/aboutUs'>
            <NavBarComponent />
            <AboutUs />
            <FooterComponent />
          </Route>
        </Switch>
      </ThemeProvider>
    </div>
  );
}

export default App;
