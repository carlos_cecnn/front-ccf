import { Grid } from '@material-ui/core';
import { Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import WebIcon from '@material-ui/icons/Web';

// Media imports
import CharlyFoto from '../images/evil_stonks_charly.png';
import AldoFoto from '../images/evil_stonks_aldo.png';
import DiegoFoto from '../images/evil_stonks_diego.png';
import FabianFoto from '../images/evil_stonks_fabian.png';
import LeoFoto from '../images/evil_stonks_leo.png';

import './WebpageStyles.css';

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(4),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
    maxWidth: "1000px",
    alignContent: "center"
  },
  images: {
    maxHeight: "250px",
    maxWidth: "400px",
  },
}));

function hasWebsite(website) {
  if (website) {
    return (
      <a href={website}>
        <WebIcon fontSize='large' />
      </a>
    );
  }
}

function InformationDetailed(props) {
  const classes = useStyles();
  const { image, name, career, linkedInProfile, webSite } = props

  return (
    <div align="center">
      <Paper className={classes.paper}>
        <Grid container>
          <Grid item xs={10} sm={5}>
            <img alt={name} className={classes.images} src={image} />
          </Grid>
          <Grid item xs={2} sm={1} />
          <Grid item xs={12} sm={6}>
            <Typography variant='h2'>
              {name}
            </Typography>
            <br />
            <Typography variant='h4'>
              {career}
            </Typography>
            <br />
            <Typography variant='h4'>
              <a href={linkedInProfile}>
                <LinkedInIcon fontSize='large' />
              </a>
              {hasWebsite(webSite)}
            </Typography>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
}



export default function AboutUs() {

  return (
    <div className="AboutStyles">
      <br />
      {/* Carlos Carbajal information */}
      <InformationDetailed
        image={CharlyFoto}
        name="Carlos Carbajal Nogués"
        career="Ingeniería en Sistemas Computacionales (ISC)"
        linkedInProfile="https://www.linkedin.com/in/carlos-cecnn12"
        webSite="http://www.carlos-cecnn.space"
      />

      <br />

      {/* Aldo Turégano information */}
      <InformationDetailed
        image={AldoFoto}
        name="Aldo Turégano Montes"
        career="Ingeniería Mecatrónica (IMT)"
        linkedInProfile="https://www.linkedin.com/in/aldo-turégano-montes-224952112"
      />

      <br />

      {/* Diego Navarrete information */}
      <InformationDetailed
        image={DiegoFoto}
        name="Diego Hernán Navarrete Verver"
        career="Licenciatura en Administración de Empresas (LAE)"
        linkedInProfile="https://www.linkedin.com/in/diego-hernán-navarrete-verver-18166914a"
      />

      <br />

      {/* Fabián Camp information */}
      <InformationDetailed
        image={FabianFoto}
        name="Fabián Camp Mussa"
        career="Ingeniería en Sistemas Computacionales (ISC)"
        linkedInProfile="https://www.linkedin.com/in/fabián-camp-mussa-2a8a691b1/"
        webSite="https://fabshub.net"
      />

      <br />

      {/* Leo Valencia information */}
      <InformationDetailed
        image={LeoFoto}
        name="Leonardo Valencia Benítez"
        career="Ingeniería en Sistemas Digitales (ISD)"
        linkedInProfile="https://www.linkedin.com/in/leonardo-valencia-benitez-62644215b/"
      />

      <br />

    </div >
  );
}