import React, { useState, useEffect } from 'react';
import { Table, TableCell, TableContainer, TableHead, TableSortLabel } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import { TableBody } from '@material-ui/core';
import { TableRow } from '@material-ui/core';
import { Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import AlertDialog from './AlertDialog';
import './WebpageStyles.css'

const axios = require('axios');

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}


const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  root: {
    width: '100%',
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

const headCells = [
  { id: 'ID_trans', numeric: false, disablePadding: true, label: 'Número de Transacción' },
  { id: 'nombre', numeric: false, disablePadding: false, label: 'Nombre' },
  { id: 'CC_num', numeric: false, disablePadding: false, label: 'Tarjeta de Crédito' },
  { id: 'Fraud', numeric: false, disablePadding: false, label: 'Es fraude' },
  { id: 'verMas', numeric: false, disablePadding: false, label: 'Ver Más' },
]

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (

    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
};

export default function CreditFraudComponent() {
  const [data, setData] = useState([]);
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('Fraud');

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        'http://18.191.169.47:8080/allTransactions',
      );
 
      setData(result.data);
    };
 
    fetchData();
  }, []);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  

  return (
    <div className='CreditFraud'>
      <br />
      <br />
      <Grid container justify="{center}">
        <Grid item xs={2} sm={1} />
        <Grid item xs={20} sm={10}>
          <Paper className="container">
            <TableContainer>
              <Table
                className={classes.table}
                aria-label="Fraud Transactions Table"
                size='medium'
              >
                <EnhancedTableHead
                  classes={classes}
                  order={order}
                  orderBy={orderBy}
                  onRequestSort={handleRequestSort}
                />
                <TableBody>
                  {stableSort(data, getComparator(order, orderBy))
                    .map((row, index) => {
                      return (
                        <TableRow>
                          <TableCell component="th" scope="row">{row.ID_trans}</TableCell>
                          <TableCell>{row.First} {row.Last}</TableCell>
                          <TableCell>{row.CC_num}</TableCell>
                          <TableCell>{row.Fraud === 1 ? "True" : "False"}</TableCell>
                          <TableCell>
                            <AlertDialog
                              amt={row.Amt}
                              category={row.Cat}
                              location={row.Location}
                              store={row.Merchant}
                              unixTime={row.DateTime}
                            />
                          </TableCell>
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </Grid>
        <Grid item xs={2} sm={1} />
      </Grid>
      <br />
      <br />
    </div>
  )
}

// {dummyData.map((entry) => (
//   <TableRow>
//     <TableCell>{entry.idTransaction}</TableCell>
//     <TableCell>{entry.firstName} {entry.lastName}</TableCell>
//     <TableCell>{entry.creditCard}</TableCell>
//     <TableCell>{entry.isFraud === 1 ? "True" : "False"}</TableCell>
//     <TableCell>
//       <AlertDialog
//         amt={entry.amt}
//         category={entry.category}
//         location={entry.location}
//         store={entry.nameShop}
//         unixTime={entry.unixTime}
//       />
//     </TableCell>
//   </TableRow>
// ))}
