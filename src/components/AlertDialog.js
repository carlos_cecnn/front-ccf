import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { ListItemText } from '@material-ui/core';

export default function AlertDialog(props) {
  const [open, setOpen] = React.useState(false);

  let dateTime = props.unixTime.split(" ");
  let finalDate = dateTime[0].split("-");
  let finalHour = dateTime[1].split(":");
  const stringDate = finalDate[2] + "/" +
                   finalDate[1] + "/" +
                   finalDate[0]
                  ;
  const stringHour = finalHour[0] + ":" +
                    finalHour[1] + ":" +
                    finalHour[2]
                  ;
  

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Ver más
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Detalles de la transacción"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <ListItemText>Monto: ${props.amt}</ListItemText>
            <ListItemText>Fecha: {stringDate}</ListItemText>
            <ListItemText>Hora: {stringHour}</ListItemText>
            <ListItemText>Tienda: {props.store}</ListItemText>
            <ListItemText>Categoría de la compra: {props.category}</ListItemText>
            <ListItemText>Dirección de la tienda: {props.location}</ListItemText>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Cerrar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}