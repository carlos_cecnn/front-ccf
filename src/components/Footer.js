import React from 'react';
import { Typography, Divider } from '@material-ui/core';
import './FooterPage.css';

export default function FooterComponent() {
    return (
        <footer>
            <Divider />
            <br />
            <Typography variant='body2' color='primary'>
                © {new Date().getFullYear()} por Linces Consultores Auuuuu
            </Typography>
            <Typography variant='body2' color='primary'>
                Desarrollado con <a className='Footer-link' href='https://es.reactjs.org'>React</a> y <a 
                className='Footer-link' href='https://material-ui.com'>Material UI</a>
            </Typography>
        </footer>
    );
}